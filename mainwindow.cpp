#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cstdio>
#include <iostream>
#include <sstream>
#include <math.h>
#include <QDir>
using namespace std;

#include <QResizeEvent>
#include <QPaintEngine>
#include <QLine>
#include <QSize>
#include <QCursor>

#include "fuzzy/fuzzyelement.h"
#include "fuzzy/signal.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	my_model(1),
	my_network(),
	ui(new Ui::MainWindow),
	network_size(),
	scale(1),
	dont_repaint(false)
{
	Signal::scale = &scale;
	FILE* f = fopen("/home/ok1asw/prog/viz/data/neural_model", "r");
	auto t = QDir::currentPath();
	my_model.load(f);
	fclose(f);
	my_network.from_model(&my_model);

	ui->setupUi(this);
	recalc_size();
	QList<QAction*> actions;
	QMenu* menu = ui->menuBar->addMenu("Periods");
	for (unsigned int i = 0; i < my_network.periods.size(); ++i) {
		QString s = Signal::time_to_string(my_network.periods[i]);
		QAction* action = new QAction(s, menu);
		action->setData(QVariant(my_network.periods[i]));
		connect(action, SIGNAL(triggered()), this, SLOT(set_period()));
		actions.push_back(action);
	}
	menu->addActions(actions);

	ui->statusBar->showMessage("Ready");
}

void MainWindow::set_period() {
	QAction* action = qobject_cast<QAction*>(sender());
	if (!action) {
		return;
	}
	if (!action->data().isNull()) {
		double data = qvariant_cast<double>(action->data());
		ui->statusBar->showMessage("period: " + QString::number(data) + "s");
		Signal::period = data;
		repaint();
	}
}

void MainWindow::recalc_size() {
	network_size = my_network.get_size();
	qreal sw = ui->sbar_v->pos().x();
	qreal sh = ui->sbar_h->pos().y();
	ui->sbar_h->setMaximum(std::max(int(network_size.width()*scale - sw), 0));
	ui->sbar_v->setMaximum(std::max(int(network_size.height()*scale - sh), 0));
}

void MainWindow::paintEvent(QPaintEvent *event) {
	Q_UNUSED(event);
	QPainter qp(this);

	QRectF cr(0, 0, ui->sbar_v->pos().x(), ui->sbar_h->pos().y()+ui->menuBar->height());
	qp.setClipRect(cr);
	qp.setBrush(QColor(255, 255, 255));
	qp.setPen(QColor(255, 255, 255));
	qp.drawRect(cr);

	qp.scale(scale, scale);
	qp.translate(-ui->sbar_h->value()/scale, -ui->sbar_v->value()/scale + (ui->menuBar->height())/scale);

	my_network.paint(qp, QRectF(QPointF(ui->sbar_h->value(), ui->sbar_v->value() - (ui->menuBar->height()))/scale, cr.size()/scale));

	qp.resetTransform();
	qp.translate(10, ui->sbar_h->pos().y()+ui->menuBar->height()-30);
	for (int i = 0; i <= 100; ++i) {
		QColor colour = Signal::value_to_colour(i / 100.0);
		qp.setPen(colour);
		qp.drawLine(i, 0, i, 20);
		if (i % 50 == 0) {
			qp.setPen(QColor(0, 0, 0));
			qp.drawLine(i, -5, i, -1);
		}
	}
	QFont font = qp.font();
	font.setPointSize(12);
	qp.setFont(font);
	qp.setPen(QColor(0, 0, 0));
	qp.drawText(-3, -6, "0");
	qp.drawText(40, -6, "0.5");
	qp.drawText(97, -6, "1");
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event) {
	Q_UNUSED(event);
	QSize w_size = ui->centralWidget->size();
	QSize h_size = ui->sbar_h->size();
	QSize v_size = ui->sbar_v->size();
	ui->sbar_h->resize(w_size.width()-v_size.width(), h_size.height());
	ui->sbar_h->move(0, w_size.height()-h_size.height());
	ui->sbar_v->resize(v_size.width(), w_size.height()-h_size.height());
	ui->sbar_v->move(w_size.width()-v_size.width(), 0);
	recalc_size();
}

void MainWindow::on_sbar_h_valueChanged(int value)
{
	Q_UNUSED(value);
	if (!dont_repaint) {
		repaint();
	}
}

void MainWindow::on_sbar_v_valueChanged(int value)
{
	Q_UNUSED(value);
	if (!dont_repaint) {
		repaint();
	}
}

void MainWindow::scale_scrollbars(qreal s, bool mouse) {
	QPoint pos1 = QPoint(ui->sbar_h->value(), ui->sbar_v->value());
	QPoint sm;
	if (mouse) {
		sm = ui->centralWidget->mapFromGlobal(QCursor::pos());
	} else {
		QSize w_size = ui->centralWidget->size();
		sm = QPoint((w_size.width()-ui->sbar_v->size().width())/2, (w_size.height()-ui->sbar_h->size().height())/2);
	}
	QPoint pos2 = (pos1 + sm)*s - sm;
	recalc_size();
	dont_repaint = true;
	ui->sbar_h->setValue(pos2.x());
	ui->sbar_v->setValue(pos2.y());
	dont_repaint = false;
}

void MainWindow::on_actionZoom_In_triggered()
{
	qreal s = scale;
	scale *= 1.4;
	if (scale > 5) {
		scale = 5;
	}
	s = scale / s;
	scale_scrollbars(s, false);
	repaint();
}

void MainWindow::on_actionZoom_Out_triggered()
{
	qreal s = scale;
	scale /= 1.4;
	if (scale < 0.05) {
		scale = 0.05;
	}
	s = scale / s;
	scale_scrollbars(s, false);
	repaint();
}

void MainWindow::wheelEvent(QWheelEvent *event) {
	int numPixels = event->angleDelta().y();
	if (event->modifiers().testFlag(Qt::ControlModifier)) {
		qreal sc = pow(1.0005, numPixels);
		qreal s = scale;
		scale *= sc;
		if (scale > 5) {
			scale = 5;
		} else if (scale < 0.05) {
			scale = 0.05;
		}
		s = scale / s;
		scale_scrollbars(s, true);
		repaint();
	} else if (event->modifiers().testFlag(Qt::ShiftModifier)) {
		ui->sbar_h->setValue(ui->sbar_h->value() - numPixels);
	} else {
		ui->sbar_v->setValue(ui->sbar_v->value() - numPixels);
	}
	event->accept();
}
