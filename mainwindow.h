#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSizeF>
#include "neural/neuronmodel.h"
#include "fuzzy/fuzzynetwork.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = 0);
		~MainWindow();
		NeuronModel my_model;
		FuzzyNetwork my_network;

	protected:
		void paintEvent(QPaintEvent *event);
		void resizeEvent(QResizeEvent *event);
		void wheelEvent(QWheelEvent *event);

	private slots:
		void on_sbar_h_valueChanged(int value);
		void on_sbar_v_valueChanged(int value);

		void on_actionZoom_In_triggered();
		void on_actionZoom_Out_triggered();
		void set_period();

	private:
		Ui::MainWindow *ui;

		QSizeF network_size;
		qreal scale;
		bool dont_repaint;
		void recalc_size();
		void scale_scrollbars(qreal s, bool mouse);
};

#endif // MAINWINDOW_H
