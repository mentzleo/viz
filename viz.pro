#-------------------------------------------------
#
# Project created by QtCreator 2021-04-15T14:24:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = viz
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    neural/liftinglayer.cpp \
    neural/linearlayer.cpp \
    neural/logreglayer.cpp \
    neural/minmaxlayer.cpp \
    neural/neuronlayer.cpp \
    neural/neuronmodel.cpp \
    neural/squareerror.cpp \
    fuzzy/fuzzyelement.cpp \
    fuzzy/signal.cpp \
    fuzzy/negation.cpp \
    fuzzy/conjunction.cpp \
    fuzzy/disjunction.cpp \
    fuzzy/perceptron.cpp \
    fuzzy/source.cpp \
    fuzzy/weightedaverage.cpp \
    fuzzy/output.cpp \
    fuzzy/fuzzynetwork.cpp \
    fuzzy/justline.cpp

HEADERS += \
        mainwindow.h \
    neural/liftinglayer.h \
    neural/linearlayer.h \
    neural/logreglayer.h \
    neural/minmaxlayer.h \
    neural/neuronlayer.h \
    neural/neuronmodel.h \
    neural/squareerror.h \
    fuzzy/fuzzyelement.h \
    fuzzy/signal.h \
    fuzzy/negation.h \
    fuzzy/conjunction.h \
    fuzzy/disjunction.h \
    fuzzy/perceptron.h \
    fuzzy/source.h \
    fuzzy/weightedaverage.h \
    fuzzy/output.h \
    viewtransform.h \
    fuzzy/fuzzynetwork.h \
    fuzzy/justline.h

FORMS += \
        mainwindow.ui

DISTFILES += \
    neural/liftinglayer.o \
    neural/linearlayer.o \
    neural/logreglayer.o \
    neural/minmaxlayer.o \
    neural/neuronlayer.o \
    neural/neuronmodel.o \
    neural/squareerror.o \
    neural/liftinglayer.d \
    neural/linearlayer.d \
    neural/logreglayer.d \
    neural/minmaxlayer.d \
    neural/neuronlayer.d \
    neural/neuronmodel.d \
    neural/squareerror.d
