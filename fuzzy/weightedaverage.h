#ifndef WEIGHTEDAVERAGE_H
#define WEIGHTEDAVERAGE_H

#include "fuzzyelement.h"

class WeightedAverage : public FuzzyElement
{
	public:
		WeightedAverage(std::vector<FuzzyElement*> elements, std::vector<double> weights_);

		virtual void calculate_signal();

		virtual void paint(QPainter& qp, QRectF window);

		virtual void propagate_invisibility();
		virtual void propagate_flow();

	private:
		std::vector<double> weights;
		double sum_weights;
		double max_weight;

		class FeW {
			public:
				FeW(FuzzyElement* fe_, double w_);
				FuzzyElement* fe;
				double w;
				bool operator <(const FeW& o) const;
				static bool flow_order(const FeW& t, const FeW& o);
		};

		void order_by_flows();

	protected:
		virtual qreal thickness_to_parent(int i);
};

#endif // WEIGHTEDAVERAGE_H
