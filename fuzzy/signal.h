#ifndef SIGNAL_H
#define SIGNAL_H

#include <vector>
#include <QPainter>
#include <QPointF>
#include <QColor>
#include <QString>

#define SIGNAL_SIZE 100
#define SIGNAL_INNER_SIZE 50
#define SIGNAL_SPACING 2

class Signal
{
	public:
		Signal(double from, double to, double step);
		Signal();

		static double period;
		static qreal* scale;

		std::vector<double> data;

		double get_step() const;
		void set_step(double ts);
		void resize(unsigned int size);
		unsigned int size() const;

		double& operator[](unsigned int i);
		const double& operator[](unsigned int i) const;

		void paint(QPainter& qp, QPointF pos, QRectF window);

		double maxdist(const Signal& other) const;
		bool operator <=(const Signal& other) const;
		bool operator >=(const Signal& other) const;

		static QColor from_HCL(double h, double c, double l);
		static QColor value_to_colour(double val);
		static QString time_to_string(double time);

		QColor get_mean_colour();
		void calc_mean();

	private:
		double time_step;
		double mean;
		QPointF from_radial(qreal radius, qreal angle);
};

#endif // SIGNAL_H
