#ifndef JUSTLINE_H
#define JUSTLINE_H

#include "fuzzyelement.h"

class JustLine : public FuzzyElement
{
	public:
		JustLine(FuzzyElement* p);

		virtual QSizeF get_size() const;

		virtual void set_input_signal(const Signal& signal);
		virtual void calculate_signal();

		virtual void paint(QPainter& qp, QRectF window);

};

#endif // JUSTLINE_H
