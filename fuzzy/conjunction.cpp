#include "conjunction.h"

#include <assert.h>
#include <algorithm>

Conjunction::Conjunction(FuzzyElement* a, FuzzyElement* b) :
	FuzzyElement()
{
	parent.push_back(a);
	a->kids.push_back(this);
	parent.push_back(b);
	b->kids.push_back(this);
}

void Conjunction::calculate_signal() {
	assert(parent.size() == 2);
	Signal& input1 = parent[0]->get_output_signal();
	Signal& input2 = parent[1]->get_output_signal();
	output_signal.data.resize(input1.data.size());
	output_signal.set_step(input1.get_step());

	for (unsigned int i = 0; i < input1.data.size(); ++i) {
		output_signal.data[i] = std::min(input1.data[i], input2.data[i]);
	}
}

void Conjunction::paint(QPainter &qp, QRectF window) {
	FuzzyElement::paint(qp, window);
	qp.setPen(QPen(QColor(0, 0, 0)));
	draw_text_to_centre(qp, "∧", 36);
	//qp.drawText(pos + QPoint(50, 50), "∧");
}

void Conjunction::propagate_invisibility() {
	FuzzyElement::propagate_invisibility();

	if (parent.size() < 2) {
		return;
	}

	if (parent[0]->get_output_signal() <= parent[1]->get_output_signal()) {
		parent[1]->purge_kid(this);
		parent.erase(parent.begin()+1);
	} else if (parent[1]->get_output_signal() <= parent[0]->get_output_signal()) {
		parent[0]->purge_kid(this);
		parent.erase(parent.begin());
	}
}
