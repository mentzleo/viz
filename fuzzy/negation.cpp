#include "negation.h"

#include <assert.h>

Negation::Negation(FuzzyElement* p) :
	FuzzyElement()
{
	parent.push_back(p);
	p->kids.push_back(this);
}


void Negation::calculate_signal() {
	assert(parent.size() == 1);
	Signal& input = parent[0]->get_output_signal();
	output_signal.resize(input.size());
	output_signal.set_step(input.get_step());

	for (unsigned int i = 0; i < input.size(); ++i) {
		output_signal[i] = 1 - input[i];
	}
}

void Negation::paint(QPainter &qp, QRectF window) {
	FuzzyElement::paint(qp, window);
	qp.setPen(QPen(QColor(0, 0, 0)));
	draw_text_to_centre(qp, "¬", 36);
	//qp.drawText(pos + QPoint(50, 50), "¬");
}

bool Negation::can_be_dissolved() {
	return false;
}
