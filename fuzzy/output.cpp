#include "output.h"

Output::Output(FuzzyElement* p, double x_, double y_) :
	FuzzyElement(),
	x(x_),
	y(y_)
{
	parent.push_back(p);
	p->kids.push_back(this);
	flow = 1;
}

void Output::propagate_invisibility() {

}

void Output::calculate_signal() {
	output_signal = parent[0]->get_output_signal();
}

void Output::reset_flow() {
	flow = 1;
}
