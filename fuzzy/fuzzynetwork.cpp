#include "fuzzynetwork.h"

#include <algorithm>
#include <math.h>
#include "fuzzyelement.h"
#include "justline.h"
#include "conjunction.h"
#include "disjunction.h"
#include "negation.h"
#include "output.h"
#include "perceptron.h"
#include "source.h"
#include "weightedaverage.h"
#include "neural/neuronmodel.h"
#include "neural/neuronlayer.h"
#include "neural/liftinglayer.h"
#include "neural/linearlayer.h"
#include "neural/logreglayer.h"
#include "neural/minmaxlayer.h"

#define VSPACING 100
#define HSPACING 10
#define PADDING 16

#include <iostream>
#include <sstream>

FuzzyNetwork::FuzzyNetwork() :
	layers(),
	periods()
{

}

FuzzyNetwork::~FuzzyNetwork() {
	layers.clear();
}

FuzzyNetwork::Layer::Layer() :
	elements(),
	dense(false),
	u(2.0)
{

}

FuzzyNetwork::Layer::~Layer() {
	elements.clear();
}

QSizeF FuzzyNetwork::Layer::get_size() const {
	qreal w = PADDING*2;
	qreal h = 0;
	for (const std::shared_ptr<FuzzyElement>& el : elements) {
		if (!el->is_visible()) {
			continue;
		}
		QSizeF s = el->get_size();
		w += s.width() + HSPACING;
		h = std::max(h, s.height());
	}
	if (elements.size() > 1) {
		w -= HSPACING;
	}
	return QSizeF(w, h);
}

qreal FuzzyNetwork::Layer::get_spacing_above() {
	qreal max_span = 0;
	for (const std::shared_ptr<FuzzyElement>& el : elements) {
		qreal xmin = 2000000000;
		qreal xmax = -2000000000;
		for (FuzzyElement* p : el->parent) {
			qreal px = p->get_attach_bottom().x();
			xmin = std::min(xmin, px);
			xmax = std::max(xmax, px);
		}
		max_span = std::max(max_span, xmax - xmin);
	}
	return std::max(qreal(VSPACING), max_span/6);
}

void FuzzyNetwork::Layer::arange(qreal y, qreal target_width) {
	unsigned int i = 0;
	target_width -= PADDING*2;
	qreal x = PADDING;
	qreal tight_size = 0;
	int visible_elements = 0;

	for (const std::shared_ptr<FuzzyElement>& el : elements) {
		if (!el->is_visible()) {
			continue;
		}
		++visible_elements;
		QSizeF s = el->get_size();
		tight_size += s.width();
	}
	if (visible_elements == 0) {
		return;
	}
	qreal spacing = 0;
	spacing = (target_width - tight_size) / visible_elements;
	x += spacing / 2;
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		if (!el->is_visible()) {
			continue;
		}
		el->id = i;
		el->set_pos(QPointF(x, y));
		x += el->get_size().width() + spacing;
		++i;
	}

	return;
}

QPointF FuzzyNetwork::Layer::logpoint(double x, double y, double ym, double w_) const {
	float x_ = log(x*x + y*y)/2;
	float y_ = atan2(y, x);
	return QPointF(qreal(x_/log(u)*w_), qreal(y_/log(u)*w_) + ym);
}

void FuzzyNetwork::Layer::paint(QPainter &qp, QRectF window) {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		if (el->is_visible()) {
			el->paint(qp, window);
		}
	}
}

void FuzzyNetwork::Layer::paint_lines(QPainter &qp) {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		if (el->is_visible()) {
			el->paint_lines(qp);
		}
	}
}

void FuzzyNetwork::Layer::propagate_flow() {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		el->propagate_flow();
	}
}

void FuzzyNetwork::Layer::reset_flow() {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		el->reset_flow();
	}
}

void FuzzyNetwork::Layer::all_visible() {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		el->set_visibe(true);
	}
}

void FuzzyNetwork::Layer::propagate_invisibility() {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		el->propagate_invisibility();
	}
}

void FuzzyNetwork::Layer::calculate() {
	for (std::shared_ptr<FuzzyElement>& el : elements) {
		el->calculate_signal();
		el->calc_mean();
	}
}

void FuzzyNetwork::all_visible() {
	for (std::shared_ptr<Layer>& l : layers) {
		l->all_visible();
	}
}

void FuzzyNetwork::propagate_invisibility() {
	for (int i = layers.size()-1; i >= 0; --i) {
		layers[i]->propagate_invisibility();
	}
}

void FuzzyNetwork::calculate_all() {
	for (unsigned int i = 0; i < layers.size(); ++i) {
		layers[i]->calculate();
	}
}

void FuzzyNetwork::reset_flow() {
	for (std::shared_ptr<Layer>& l : layers) {
		l->reset_flow();
	}
}

void FuzzyNetwork::propagate_flow() {
	for (int i = layers.size()-1; i >= 0; --i) {
		layers[i]->propagate_flow();
	}
}

bool FuzzyNetwork::fe_sort(const std::shared_ptr<FuzzyElement> &e1, const std::shared_ptr<FuzzyElement> &e2) {
	return e1->id < e2->id;
}

void FuzzyNetwork::purge_invisible() {
	for (unsigned int i = 0; i < layers.size(); ++i) {
		for (unsigned int j = 0; j < layers[i]->elements.size(); ++j) {
			if (!layers[i]->elements[j]->is_visible()) {
				layers[i]->elements.erase(layers[i]->elements.begin() + j);
				--j;
			}
		}
	}
}

void FuzzyNetwork::optimise_crossings() {
	for (unsigned int i = 0; i < layers.size(); ++i) {
		for (unsigned int j = 0; j < layers[i]->elements.size(); ++j) {
			layers[i]->elements[j]->id = j;
		}
	}

	for (int iter = 0; iter < 1000; ++iter) {
		bool brk = true;
		for (int l = layers.size()-2; l >= 0; --l) {
			Layer* layer = layers[l].get();
			for (unsigned int i = 0; i < layer->elements.size(); ++i) {
				int kl = layer->elements.size();
				int kr = -1;
				for (FuzzyElement* k : layer->elements[i]->kids) {
					kl = std::min(kl, k->id);
					kr = std::max(kr, k->id);
				}
				float klr = (kl + kr) / 2.0;
				for (unsigned int j = 0; j < layer->elements.size(); ++j) {
					int kl_ = layer->elements.size();
					int kr_ = -1;
					for (FuzzyElement* k_ : layer->elements[j]->kids) {
						kl_ = std::min(kl_, k_->id);
						kr_ = std::max(kr_, k_->id);
					}
					float klr_ = (kl_ + kr_) / 2.0;
					if (layer->elements[i]->id > layer->elements[j]->id) {
						if (klr < klr_) {
							brk = false;
							std::swap(layer->elements[i]->id, layer->elements[j]->id);
						}
					} else {
						if (klr_ < klr) {
							brk = false;
							std::swap(layer->elements[i]->id, layer->elements[j]->id);
						}
					}
				}
			}
		}
		for (unsigned int l = 1; l < layers.size(); ++l) {
			Layer* layer = layers[l].get();
			for (unsigned int i = 0; i < layer->elements.size(); ++i) {
				int pl = layer->elements.size();
				int pr = -1;
				for (FuzzyElement* p : layer->elements[i]->parent) {
					pl = std::min(pl, p->id);
					pr = std::max(pr, p->id);
				}
				float plr = (pl + pr) / 2.0;
				for (unsigned int j = 0; j < layer->elements.size(); ++j) {
					int pl_ = layer->elements.size();
					int pr_ = -1;
					for (FuzzyElement* p_ : layer->elements[j]->parent) {
						pl_ = std::min(pl_, p_->id);
						pr_ = std::max(pr_, p_->id);
					}
					float plr_ = (pl_ + pr_) / 2.0;
					if (layer->elements[i]->id > layer->elements[j]->id) {
						if (plr < plr_) {
							brk = false;
							std::swap(layer->elements[i]->id, layer->elements[j]->id);
						}
					} else {
						if (plr_ < plr) {
							brk = false;
							std::swap(layer->elements[i]->id, layer->elements[j]->id);
						}
					}
				}
			}
		}
		if (brk) {
			break;
		}
	}

	for (unsigned int i = 0; i < layers.size(); ++i) {
		std::sort(layers[i]->elements.begin(), layers[i]->elements.end(), &fe_sort);
	}
}

void FuzzyNetwork::arange() {
	qreal width = get_size().width();
	qreal y = PADDING;
	for (unsigned int i = 0; i < layers.size(); ++i) {
		/*if (i && (layers[i]->elements.size() != 1) && (layers[i-1]->elements.size() != 1)) {
			unsigned int connections = 0;
			for (unsigned int j = 0; j < layers[i]->elements.size(); ++j) {
				connections += layers[i]->elements[j]->parent.size();
			}
			layers[i]->dense = float(connections) / std::min(layers[i-1]->elements.size(), layers[i]->elements.size()) > 2;
			if (layers[i]->dense) {
				y += M_PI_2*layers[i-1]->elements[layers[i-1]->elements.size()-1]->get_attach_bottom().x()/log(layers[i]->u) - VSPACING;
			}
		}*/
		QSizeF lsize = layers[i]->get_size();
		layers[i]->arange(y, width);
		if (i+1 < layers.size()) {
			y += lsize.height() + layers[i+1]->get_spacing_above();
		}
	}
}

void FuzzyNetwork::paint(QPainter &qp, QRectF window) {
	for (unsigned int i = 0; i < layers.size(); ++i) {
		layers[i]->paint_lines(qp);
	}
	for (unsigned int i = 0; i < layers.size(); ++i) {
		layers[i]->paint(qp, window);
	}
}

QSizeF FuzzyNetwork::get_size() const {
	qreal w = 0;
	qreal h = PADDING*2;
	for (unsigned int i = 0; i < layers.size(); ++i) {
		QSizeF s = layers[i]->get_size();
		w = std::max(w, s.width());
		h += s.height();
		if (i+1 < layers.size()) {
			h += layers[i+1]->get_spacing_above();
		}
		if (i && layers[i]->dense) {
			h += M_PI_2*layers[i-1]->elements[layers[i-1]->elements.size()-1]->get_attach_bottom().x()/log(layers[i]->u) - VSPACING;
		}
	}
	return QSizeF(w, h);
}

void FuzzyNetwork::from_model(NeuronModel *model) {
	std::vector<double> xs, ys;
	double min_signal = 0;
	double max_signal = 604800;
	double signal_step = 300;
	Signal time_signal(min_signal, max_signal, signal_step);

	for (unsigned int l = 0; l < model->layers.size(); ++l) {
		std::vector<double> new_xs, new_ys;
		LiftingLayer* lfl = dynamic_cast<LiftingLayer*>(model->layers[l].get());
		LinearLayer* lnl = dynamic_cast<LinearLayer*>(model->layers[l].get());
		MinMaxLayer* mml = dynamic_cast<MinMaxLayer*>(model->layers[l].get());

		if (lfl) {
			std::cerr << "importing perceptron layer" << std::endl;
			LogregLayer* lgl = dynamic_cast<LogregLayer*>(model->layers[l+1].get());
			int s = lgl->sets;
			std::shared_ptr<Layer> fl(new Layer);
			for (unsigned int i = 0; i < lfl->periods.size(); ++i) {
				periods.push_back(lfl->periods[i]);
				for (int j = 0; j < s; ++j) {
					std::shared_ptr<FuzzyElement> fe(new Perceptron(lfl->periods[i], lgl->xw[i*s + j], lgl->yw[i*s + j], lgl->zw[i*s + j]));
					fe->set_input_signal(time_signal);
					fl->elements.push_back(fe);
					new_xs.push_back(1);
					new_ys.push_back(0);
				}
			}
			layers.push_back(fl);
			++l; // We proccessed two layers at once.
			std::cerr << "imported neurons: " << fl->elements.size() << std::endl;
			std::cerr << "new xs: " << new_xs.size() << std::endl;
			std::cerr << "new ys: " << new_ys.size() << std::endl;
		} else if (lnl) {

			std::cerr << "importing linear layer" << std::endl;
			std::shared_ptr<Layer> negs(new Layer);
			std::shared_ptr<Layer> fl(new Layer);
			const std::vector<std::shared_ptr<FuzzyElement> >& last_layer = layers[layers.size()-1]->elements;
			int in = lnl->cols-1;
			for (int j = 0; j < in; ++j) {
				std::shared_ptr<FuzzyElement> pt(new JustLine(last_layer[j].get()));
				negs->elements.push_back(pt);
				std::shared_ptr<FuzzyElement> neg(new Negation(last_layer[j].get()));
				negs->elements.push_back(neg);
			}
			for (int i = 0; i < lnl->rows; ++i) {
				std::vector<FuzzyElement*> parents;
				std::vector<double> weights;
				double x = 0;
				double y = lnl->W[i*lnl->cols + in];
				for (int j = 0; j < in; ++j) {
					double w = lnl->W[i*lnl->cols + j];
					if (w < 0) {
						parents.push_back(negs->elements[2*j + 1].get());
						weights.push_back(-w * xs[j]);
						x += -w * xs[j];
						y += w * (xs[j] + ys[j]);
					} else {
						parents.push_back(negs->elements[2*j].get());
						weights.push_back(w * xs[j]);
						x += w * xs[j];
						y += w * ys[j];
					}
				}
				y += lnl->W[i*lnl->cols + in];
				std::shared_ptr<FuzzyElement> fe(new WeightedAverage(parents, weights));
				fl->elements.push_back(fe);
				new_xs.push_back(x);
				new_ys.push_back(y);
			}
			layers.push_back(negs);
			layers.push_back(fl);
			std::cerr << "imported neurons: " << negs->elements.size() << ", " << fl->elements.size() << std::endl;
			std::cerr << "new xs: " << new_xs.size() << std::endl;
			std::cerr << "new ys: " << new_ys.size() << std::endl;

		} else if (mml) {
			// Survive this
			std::cerr << "importing min-max layer" << std::endl;
			std::shared_ptr<Layer> sl(new Layer);
			std::shared_ptr<Layer> vps(new Layer);
			std::shared_ptr<Layer> fl(new Layer);
			const std::vector<std::shared_ptr<FuzzyElement> >& last_layer = layers[layers.size()-1]->elements;
			for (unsigned int i = 0; i < xs.size(); ++i) {
				for (unsigned int j = 0; j < xs.size(); ++j) {
					if (i == j) {
						std::shared_ptr<FuzzyElement> p1(new JustLine(last_layer[i].get()));
						sl->elements.push_back(p1);
						std::shared_ptr<FuzzyElement> p2(new JustLine(p1.get()));
						vps->elements.push_back(p2);
						std::shared_ptr<FuzzyElement> p3(new JustLine(p2.get()));
						fl->elements.push_back(p3);
						new_xs.push_back(xs[i]);
						new_ys.push_back(ys[i]);
					} else {

						double x1 = xs[i];
						double x2 = xs[j];
						double y1 = ys[i];
						double y2 = ys[j];
						FuzzyElement* s1 = last_layer[i].get();
						FuzzyElement* s2 = last_layer[j].get();
						FuzzyElement* o1;
						FuzzyElement* o2;
						bool swapped = false;

						if (y2 > y1) {
							std::swap(x1, x2);
							std::swap(y1, y2);
							std::swap(s1, s2);
							swapped = true;
						}

						if (x1 + y1 - y2 >= x2) {
							std::shared_ptr<FuzzyElement> p1(new JustLine(s1));
							sl->elements.push_back(p1);
							std::shared_ptr<FuzzyElement> one(new Source(1, signal_step, time_signal.size()));
							sl->elements.push_back(one);
							std::shared_ptr<FuzzyElement> p2(new JustLine(s2));
							sl->elements.push_back(p2);
							std::shared_ptr<FuzzyElement> zero(new Source(0, signal_step, time_signal.size()));
							sl->elements.push_back(zero);

							std::vector<FuzzyElement*> vpe1;
							vpe1.push_back(p1.get());
							vpe1.push_back(one.get());
							std::vector<double> vpv1;
							vpv1.push_back(x1);
							vpv1.push_back(y1 - y2);
							std::shared_ptr<FuzzyElement> vpe1fl(new WeightedAverage(vpe1, vpv1));
							vps->elements.push_back(vpe1fl);
							std::vector<FuzzyElement*> vpe2;
							vpe2.push_back(p2.get());
							vpe2.push_back(zero.get());
							std::vector<double> vpv2;
							vpv2.push_back(x2);
							vpv2.push_back(x1 + y1 - y2 - x2);
							std::shared_ptr<FuzzyElement> vpe2fl(new WeightedAverage(vpe2, vpv2));
							vps->elements.push_back(vpe2fl);

							o1 = vpe1fl.get();
							o2 = vpe2fl.get();
							new_xs.push_back(x1 + y1 - y2);
							new_ys.push_back(y2);
						} else {
							std::shared_ptr<FuzzyElement> p1(new JustLine(s1));
							sl->elements.push_back(p1);
							std::shared_ptr<FuzzyElement> one(new Source(1, signal_step, time_signal.size()));
							sl->elements.push_back(one);
							std::shared_ptr<FuzzyElement> zero(new Source(0, signal_step, time_signal.size()));
							sl->elements.push_back(zero);
							std::shared_ptr<FuzzyElement> p2(new JustLine(s2));
							sl->elements.push_back(p2);

							std::vector<FuzzyElement*> vpe1;
							vpe1.push_back(p1.get());
							vpe1.push_back(one.get());
							vpe1.push_back(zero.get());
							std::vector<double> vpv1;
							vpv1.push_back(x1);
							vpv1.push_back(y1 - y2);
							vpv1.push_back(x2 - x1 - y1 + y2);
							std::shared_ptr<FuzzyElement> vpe1fl(new WeightedAverage(vpe1, vpv1));
							vps->elements.push_back(vpe1fl);
							std::shared_ptr<FuzzyElement> p3(new JustLine(p2.get()));
							vps->elements.push_back(p3);

							o1 = vpe1fl.get();
							o2 = p3.get();
							new_xs.push_back(x2);
							new_ys.push_back(y2);
						}

						if (swapped) {
							std::swap(o1, o2);
						}

						if (i < j) {
							std::shared_ptr<FuzzyElement> max(new Disjunction(o1, o2));
							fl->elements.push_back(max);
						} else {
							std::shared_ptr<FuzzyElement> min(new Conjunction(o1, o2));
							fl->elements.push_back(min);
						}
					}
				}
			}
			layers.push_back(sl);
			layers.push_back(vps);
			layers.push_back(fl);
			std::cerr << "imported neurons: " << sl->elements.size() << ", " << vps->elements.size() << ", " << fl->elements.size() << std::endl;
			std::cerr << "new xs: " << new_xs.size() << std::endl;
			std::cerr << "new ys: " << new_ys.size() << std::endl;
		}
		xs = new_xs;
		ys = new_ys;
	}

	std::cerr << "importing output layer" << std::endl;
	std::shared_ptr<Layer> ol(new Layer);
	for (unsigned int i = 0; i < xs.size(); ++i) {
		std::shared_ptr<FuzzyElement> o(new Output(layers[layers.size()-1]->elements[i].get(), xs[i], ys[i]));
		std::cerr << xs[i] << " " << ys[i] << std::endl;
		ol->elements.push_back(o);
	}
	layers.push_back(ol);
	std::cerr << "imported neurons: " << ol->elements.size() << std::endl;

	calculate_all();

	propagate_flow();
	propagate_invisibility();
	dissolve_layers();
	purge_invisible();
	reset_flow();
	propagate_flow();
	optimise_crossings();
	arange();
}

void FuzzyNetwork::dissolve_layers() {
	for (int i = layers.size()-1; i >= 0; --i) {
		bool dissolve = true;
		for (unsigned int j = 0; j < layers[i]->elements.size(); ++j) {
			if (layers[i]->elements[j]->is_visible() && (!layers[i]->elements[j]->can_be_dissolved())) {
				dissolve = false;
				break;
			}
		}

		if (!dissolve) {
			continue;
		}

		for (unsigned int j = 0; j < layers[i]->elements.size(); ++j) {
			if (layers[i]->elements[j]->is_visible()) {
				layers[i]->elements[j]->dissolve();
			}
		}
		layers.erase(layers.begin()+i);
	}
}
