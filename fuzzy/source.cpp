#include "source.h"

#include <assert.h>

Source::Source(std::vector<FuzzyElement*> elements, std::vector<double> weights) :
	FuzzyElement()
{
	assert(elements.size() == weights.size());
	assert(elements.size() > 0);
	double sw = 0;
	for (unsigned int i = 0; i < weights.size(); ++i) {
		sw += weights[i];
	}
	output_signal.resize(elements[0]->get_output_signal().size());

}

Source::Source(double value, double time_step, unsigned int size) :
	FuzzyElement()
{
	output_signal.set_step(time_step);
	output_signal.resize(size);
	for (unsigned int i = 0; i < size; ++i) {
		output_signal[i] = value;
	}
}
