#include "perceptron.h"

#include <math.h>

Perceptron::Perceptron(double period_, double x_, double y_, double z_) :
	FuzzyElement(),
	period(period_),
	x(x_),
	y(y_),
	z(z_)
{

}

void Perceptron::set_input_signal(const Signal& signal) {
	output_signal.set_step(signal.get_step());
	output_signal.resize(signal.size());

	for (unsigned int i = 0; i < signal.size(); ++i) {
		double t = signal[i];
		double phase = fmodf(t, period) / period * 2 * M_PI;
		double X = cos(phase);
		double Y = sin(phase);
		output_signal[i] = 1 / (1 + exp(X*x + Y*y + z));
	}
}

void Perceptron::paint(QPainter &qp, QRectF window) {
	FuzzyElement::paint(qp, window);
	qp.setPen(QPen(QColor(0, 0, 0)));
	draw_text_to_centre(qp, Signal::time_to_string(period), 12);
}
