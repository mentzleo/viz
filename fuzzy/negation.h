#ifndef NEGATION_H
#define NEGATION_H

#include "fuzzyelement.h"

class Negation : public FuzzyElement
{
	public:
		Negation(FuzzyElement* p);

		virtual void calculate_signal();

		virtual bool can_be_dissolved();

		virtual void paint(QPainter& qp, QRectF window);
};

#endif // NEGATION_H
