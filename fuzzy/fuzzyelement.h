#ifndef FUZZYELEMENT_H
#define FUZZYELEMENT_H

#include <vector>
#include <QPointF>
#include <QPainter>
#include <QSizeF>
#include <QRectF>
#include <QColor>
#include <QPen>

#include "signal.h"

#define BASE_THICKNESS 20.0

class FuzzyElement
{
	public:
		FuzzyElement();
		virtual ~FuzzyElement() {}

		std::vector<FuzzyElement*> parent;
		std::vector<FuzzyElement*> kids;
		int id;
		double flow;

		virtual QPointF get_attach_bottom() const;
		virtual QPointF get_attach_top() const;
		virtual QSizeF get_size() const;

		void set_pos(QPointF pos_);
		bool is_visible() const;
		void set_visibe(bool v);
		virtual void propagate_invisibility();
		virtual bool can_be_dissolved();
		virtual void dissolve();
		void purge_kid(FuzzyElement* k);
		virtual void propagate_flow();
		virtual void reset_flow();

		inline void calc_mean() {
			output_signal.calc_mean();
		}

		inline QColor get_mean_colour() {
			return output_signal.get_mean_colour();
		}

		virtual void set_input_signal(const Signal& signal) {Q_UNUSED(signal)}
		virtual void calculate_signal() {}
		Signal& get_output_signal();

		virtual void paint(QPainter& qp, QRectF window);
		void paint_lines(QPainter& qp);

	protected:
		Signal output_signal;
		QPointF pos;
		bool visible;

		virtual qreal thickness_to_parent(int i);
		QPen pen_to_parent(int i);
		void draw_text_to_centre(QPainter& qp, QString s, int size);
};

#endif // FUZZYELEMENT_H
