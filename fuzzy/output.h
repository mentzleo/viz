#ifndef OUTPUT_H
#define OUTPUT_H

#include "fuzzyelement.h"

class Output : public FuzzyElement
{
	public:
		Output(FuzzyElement* p, double x_, double y_);

		virtual void propagate_invisibility();
		virtual void calculate_signal();
		virtual void reset_flow();

	private:
		double x, y;
};

#endif // OUTPUT_H
