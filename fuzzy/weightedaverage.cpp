#include "weightedaverage.h"

#include <assert.h>
#include <iostream>
#include <sstream>

#include <QString>

WeightedAverage::WeightedAverage(std::vector<FuzzyElement*> elements, std::vector<double> weights_) :
	FuzzyElement(),
	weights(weights_),
	sum_weights(0),
	max_weight(0)
{
	assert(elements.size() == weights_.size());
	assert(elements.size() > 0);
	parent = elements;

	for (double w : weights_) {
		sum_weights += w;
		max_weight = std::max(max_weight, w);
	}

	if (sum_weights == 0) {
		sum_weights = 1;
	}

	for (FuzzyElement* p : parent) {
		p->kids.push_back(this);
	}
}

void WeightedAverage::order_by_flows() {
	std::vector<FeW> fews;
	for (unsigned int i = 0; i < parent.size(); ++i) {
		fews.push_back(FeW(parent[i], weights[i]));
	}
	std::sort(fews.begin(), fews.end(), &FeW::flow_order);
	parent.clear();
	weights.clear();

	for (unsigned int i = 0; i < fews.size(); ++i) {
		parent.push_back(fews[i].fe);
		weights.push_back(fews[i].w);
	}
}

void WeightedAverage::propagate_invisibility() {
	FuzzyElement::propagate_invisibility();
	if (!visible) {
		return;
	}

	order_by_flows();
	for (unsigned int i = 0; i < parent.size(); ++i) {
		std::vector<unsigned int> to_purge;
		for (unsigned int j = i+1; j < parent.size(); ++j) {
			//std::cout << i << " " << j << " " << parent.size() << std::endl;
			double d = parent[i]->get_output_signal().maxdist(parent[j]->get_output_signal());
			if (d < 0.05) {
				//std::cout << d << std::endl;
				/*if (weights[i] < weights[j]) {
					std::swap(weights[i], weights[j]);
					std::swap(parent[i], parent[j]);
				}*/
				/*weights[i] += weights[j];
				parent[j]->purge_kid(this);
				parent.erase(parent.begin() + j);
				--j;*/
				to_purge.push_back(j);
			}
			for (int k = to_purge.size()-1; k >= 0; --k) {
				weights[i] += weights[to_purge[k]];
				parent[to_purge[k]]->purge_kid(this);
				parent.erase(parent.begin() + to_purge[k]);
				weights.erase(weights.begin() + to_purge[k]);
			}
		}
	}

	std::vector<FeW> fews;
	for (unsigned int i = 0; i < parent.size(); ++i) {
		fews.push_back(FeW(parent[i], weights[i]));
	}
	std::sort(fews.begin(), fews.end());

	double nsw = 0.9f * sum_weights;
	sum_weights = 0;
	max_weight = 0;
	parent.clear();
	weights.clear();

	for (unsigned int i = 0; i < fews.size(); ++i) {
		if ((nsw <= 0)) {
			fews[i].fe->purge_kid(this);
		} else {
			nsw -= fews[i].w;
			parent.push_back(fews[i].fe);
			sum_weights += fews[i].w;
			max_weight = std::max(max_weight, fews[i].w);
			weights.push_back(fews[i].w);
		}
	}

	if (sum_weights == 0) {
		sum_weights = 1;
	}
}

WeightedAverage::FeW::FeW(FuzzyElement* fe_, double w_) :
	fe(fe_),
	w(w_)
{

}

bool WeightedAverage::FeW::operator <(const FeW& o) const {
	return w > o.w;
}

void WeightedAverage::calculate_signal() {
	assert(parent.size() > 0);
	output_signal.data.resize(parent[0]->get_output_signal().size());
	output_signal.set_step(parent[0]->get_output_signal().get_step());

	for (unsigned int i = 0; i < output_signal.size(); ++i) {
		double sig = 0;
		for (unsigned int j = 0; j < parent.size(); ++j) {
			sig += parent[j]->get_output_signal()[i] * weights[j];
		}
		output_signal.data[i] = sig / sum_weights;
	}
}

void WeightedAverage::paint(QPainter &qp, QRectF window) {
	FuzzyElement::paint(qp, window);
	qp.setPen(QPen(QColor(0, 0, 0)));
	draw_text_to_centre(qp, "Vp", 24);
	//qp.drawText(pos + QPoint(50, 50), "Vp");
}

void WeightedAverage::propagate_flow() {
	for (unsigned int i = 0; i < parent.size(); ++i) {
		parent[i]->flow += flow * weights[i] / sum_weights;
	}
}

bool WeightedAverage::FeW::flow_order(const FeW &t, const FeW &o) {
	return t.fe->flow > o.fe->flow;
}

qreal WeightedAverage::thickness_to_parent(int i) {
	return BASE_THICKNESS * flow * weights[i] / sum_weights;
}
