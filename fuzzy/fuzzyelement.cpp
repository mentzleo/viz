#include "fuzzyelement.h"

#include <iostream>
#include <sstream>

#include <QPen>
#include <QColor>

FuzzyElement::FuzzyElement() :
	parent(),
	kids(),
	id(0),
	flow(0),
	output_signal(),
	pos(),
	visible(true)
{

}

void FuzzyElement::set_pos(QPointF pos_) {
	pos = pos_;
}

QPointF FuzzyElement::get_attach_bottom() const {
	return QPointF(SIGNAL_SIZE/2, SIGNAL_SIZE) + pos;
}

QPointF FuzzyElement::get_attach_top() const {
	return QPointF(SIGNAL_SIZE/2, 0) + pos;
}

QSizeF FuzzyElement::get_size() const {
	return QSizeF(SIGNAL_SIZE, SIGNAL_SIZE);
}

Signal& FuzzyElement::get_output_signal() {
	return output_signal;
}

bool FuzzyElement::is_visible() const {
	return visible;
}

QPen FuzzyElement::pen_to_parent(int i) {
	/*qreal thickness = thickness_to_parent(i) * 3;
	if (thickness < 3) {
		QPen result(parent[i]->get_mean_colour(), 3.0);
		QVector<qreal> dashes;
		dashes << thickness << 3;
		result.setStyle(Qt::CustomDashLine);
		result.setDashPattern(dashes);
		return result;
	} else {
		return QPen(parent[i]->get_mean_colour(), thickness);
	}*/
	return QPen(parent[i]->get_mean_colour(), 3*std::max(thickness_to_parent(i), 1.0));
}

void FuzzyElement::paint_lines(QPainter &qp) {
	for (unsigned int i = 0; i < parent.size(); ++i) {
		qp.setPen(pen_to_parent(i));
		qp.drawLine(parent[i]->get_attach_bottom(), get_attach_top());
	}
}

void FuzzyElement::paint(QPainter &qp, QRectF window) {
	output_signal.paint(qp, pos, window);
}

void FuzzyElement::set_visibe(bool v) {
	visible = v;
}

void FuzzyElement::propagate_invisibility() {
	for (FuzzyElement* k : kids) {
		if (k->is_visible()) {
			return;
		}
	}
	visible = false;
	for (FuzzyElement* p : parent) {
		p->purge_kid(this);
	}
}

bool FuzzyElement::can_be_dissolved() {
	return (parent.size() == 1) && (kids.size() == 1);
}

void FuzzyElement::dissolve() {
	FuzzyElement* p = parent[0];
	FuzzyElement* k = kids[0];
	for (unsigned int i = 0; i <= p->kids.size(); ++i) {
		if (p->kids[i] == this) {
			p->kids[i] = k;
			break;
		}
	}
	for (unsigned int i = 0; i <= k->parent.size(); ++i) {
		if (k->parent[i] == this) {
			k->parent[i] = p;
			break;
		}
	}
}

void FuzzyElement::purge_kid(FuzzyElement* k) {
	for (unsigned int i = 0; i < kids.size(); ++i) {
		if (kids[i] == k) {
			kids.erase(kids.begin() + i);
			break;
		}
	}
}

void FuzzyElement::propagate_flow() {
	for (unsigned int i = 0; i < parent.size(); ++i) {
		parent[i]->flow += flow / parent.size();
	}
}

void FuzzyElement::reset_flow() {
	flow = 0;
}

qreal FuzzyElement::thickness_to_parent(int i) {
	Q_UNUSED(i);
	return BASE_THICKNESS * flow / std::max(1, int(parent.size()));
}

void FuzzyElement::draw_text_to_centre(QPainter& qp, QString s, int size) {
	QFont font = qp.font();
	font.setPointSize(size);
	qp.setFont(font);
	qp.drawText(QRectF(pos, get_size()), s, QTextOption(Qt::AlignCenter | Qt::AlignVCenter));
}
