#include "justline.h"

JustLine::JustLine(FuzzyElement* p) :
	FuzzyElement()
{
	parent.push_back(p);
	p->kids.push_back(this);
}

QSizeF JustLine::get_size() const {
	return QSizeF(100, 100);
}

void JustLine::set_input_signal(const Signal& signal) {
	output_signal = signal;
}

void JustLine::calculate_signal() {
	output_signal = parent[0]->get_output_signal();
}

void JustLine::paint(QPainter& qp, QRectF window) {
	qp.setPen(pen_to_parent(0));
	qp.drawLine(get_attach_top(), get_attach_bottom());
}
