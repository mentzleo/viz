#ifndef DISJUNCTION_H
#define DISJUNCTION_H

#include "fuzzyelement.h"

class Disjunction : public FuzzyElement
{
	public:
		Disjunction(FuzzyElement* a, FuzzyElement* b);

		virtual void calculate_signal();

		virtual void paint(QPainter& qp, QRectF window);

		virtual void propagate_invisibility();
};

#endif // DISJUNCTION_H
