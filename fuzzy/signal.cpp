#include "signal.h"

#include <QRectF>
#include <iostream>
#include <sstream>
#include <math.h>
#include <QPen>
#include <QBrush>
#include <algorithm>

double Signal::period = 86400;
qreal* Signal::scale = nullptr;

Signal::Signal(double from, double to, double step) :
	data(),
	time_step(step)
{
	double t = from;
	while (t < to) {
		data.push_back(t);
		t += step;
	}
}

Signal::Signal() :
	data(),
	time_step()
{

}

double Signal::get_step() const {
	return time_step;
}

void Signal::set_step(double ts) {
	time_step = ts;
}

void Signal::resize(unsigned int size) {
	data.resize(size);
}

double& Signal::operator[](unsigned int i) {
	return data[i];
}

const double& Signal::operator[](unsigned int i) const {
	return data[i];
}

unsigned int Signal::size() const {
	return data.size();
}

QColor Signal::from_HCL(double h, double c, double l) {
	h = h * M_PI/180;

	//qreal lab_l = l;
	double a = cos(h)*c;
	double b = sin(h)*c;

	double y = (l + 16.0)/116.0;
	double x = a/500.0 + y;
	double z = y - b/200.0;

	double x3 = x*x*x;
	double y3 = y*y*y;
	double z3 = z*z*z;

	x = ((x3 > 0.008856) ? x3 : ((x - 16.0/116.0)/7.787)) * 0.95047;
	y = ((y3 > 0.008856) ? y3 : ((y - 16.0/116.0)/7.787)) * 1.000;
	z = ((z3 > 0.008856) ? z3 : ((z - 16.0/116.0)/7.787)) * 1.08883;

	/*double x = item->x / 100.0;
	double y = item->y / 100.0;
	double z = item->z / 100.0;*/

	double r = 3.2404542*x - 1.5371385*y - 0.4985314*z;
	double g = -0.9692660*x + 1.8760108*y + 0.0415560*z;
	b = 0.0556434*x - 0.2040259*y + 1.0572252*z;

	r = ((r > 0.0031308) ? (1.055*pow(r, 1/2.4) - 0.055) : (12.92*r)) * 255.0;
	g = ((g > 0.0031308) ? (1.055*pow(g, 1/2.4) - 0.055) : (12.92*g)) * 255.0;
	b = ((b > 0.0031308) ? (1.055*pow(b, 1/2.4) - 0.055) : (12.92*b)) * 255.0;

	return QColor(std::min(255.0, std::max(0.0, r)), std::min(255.0, std::max(0.0, g)), std::min(255.0, std::max(0.0, b)));
}

QColor Signal::value_to_colour(double val) {
	/*if ((val > 0.49) && (val < 0.51)) {
		return QColor(255, 255, 255);
	}*/
	/*if (val < 0) {
		return QColor(255, 0, 0);
	} else if (val < 0.5) {
		return QColor(255, 255*2*val, 0);
	} else if (val < 1) {
		return QColor(255*2*(1-val), 192 + 63*2*(1-val), 0);
	} else {
		return QColor(0, 192, 0);
	}*/
	//val = std::min(1, std::max(0, val));
	double _val = 1-val;
	double l = _val*39.932 + val*89.909;
	double c = _val*84.457 + val*109.364;
	double h = _val*(39.499) + val*(128.303-360.0);
	return from_HCL(h, c, l);
}

QPointF Signal::from_radial(qreal radius, qreal angle) {
	return QPointF(radius*sin(angle), -radius*cos(angle));
}

void Signal::paint(QPainter& qp, QPointF pos, QRectF window) {
	if (!QRectF(pos, QSizeF(SIGNAL_SIZE, SIGNAL_SIZE)).intersects(window)) {
		return;
	}
	if (!scale) {
		return;
	}
	qreal outer_radius = SIGNAL_SIZE/2;
	qreal inner_radius = SIGNAL_INNER_SIZE/2;
	QPointF centre = pos + QPointF(outer_radius, outer_radius);
	double nperiods = time_step*data.size() / period;
	qreal stripe_width = (outer_radius - inner_radius + SIGNAL_SPACING) / (nperiods > 1 ? nperiods+1 : 1) - SIGNAL_SPACING;
	qreal radius_step = (outer_radius - inner_radius - stripe_width) / data.size();
	qreal angle_step = 2*M_PI / (period / time_step);

	qreal l = angle_step * inner_radius * (*scale);
	int data_step = std::max(1.0, floor(1.0/l));

	qp.setPen(QPen(QColor(255, 255, 255)));
	qp.setBrush(QBrush(QColor(255, 255, 255)));
	qp.drawPie(pos.x(), pos.y(), SIGNAL_SIZE, SIGNAL_SIZE, 0, 360*16);

	for (unsigned int i = 0; i < data.size(); i += data_step) {
		QColor colour = value_to_colour(data[i]);
		QPen pen(colour);
		QBrush brush(colour);
		qp.setPen(pen);
		qp.setBrush(brush);

		QPointF points[4];
		points[0] = from_radial(i*radius_step + inner_radius, i*angle_step) + centre;
		points[1] = from_radial(i*radius_step + stripe_width + inner_radius, i*angle_step) + centre;
		points[2] = from_radial((i+data_step)*radius_step + stripe_width + inner_radius, (i+data_step)*angle_step) + centre;
		points[3] = from_radial((i+data_step)*radius_step + inner_radius, (i+data_step)*angle_step) + centre;
		qp.drawConvexPolygon(points, 4);
	}
}

double Signal::maxdist(const Signal& other) const {
	double d = 0;
	for (unsigned int i = 0; i < size(); ++i) {
		d = std::max(d, abs(data[i] - other[i]));
	}
	return d;
}

bool Signal::operator <=(const Signal& other) const {
	for (unsigned int i = 0; i < size(); ++i) {
		if (data[i] > other[i]) {
			return false;
		}
	}
	return true;
}

bool Signal::operator >=(const Signal& other) const {
	for (unsigned int i = 0; i < size(); ++i) {
		if (data[i] < other[i]) {
			return false;
		}
	}
	return true;
}

void Signal::calc_mean() {
	mean = 0;
	for (double d : data) {
		mean += d;
	}
	mean /= data.size();
}

QColor Signal::get_mean_colour() {
	return value_to_colour(mean);
}

QString Signal::time_to_string(double time) {
	QString result = "";
	int days = floor(time / 86400.0);
	time -= days * 86400;
	bool s = false;
	if (days > 0) {
		result += QString::number(days) + "d";
		s = true;
	}
	int hours = floor(time / 3600.0);
	time -= hours * 3600;
	if (hours > 0) {
		if (s) {
			result += " ";
		}
		result += QString::number(hours) + "h";
		s = true;
	}
	int minutes = floor(time / 60.0);
	time -= minutes * 60;
	if (minutes > 0) {
		if (s) {
			result += " ";
		}
		result += QString::number(minutes) + "m";
		s = true;
	}
	if (time > 0) {
		if (s) {
			result += " ";
		}
		result += QString::number(time) + "s";
		s = true;
	}
	return result;
}
