#ifndef SOURCE_H
#define SOURCE_H

#include "fuzzyelement.h"

class Source : public FuzzyElement
{
	public:
		Source(std::vector<FuzzyElement*> elements, std::vector<double> weights);
		Source(double value, double time_step, unsigned int size);
};

#endif // SOURCE_H
