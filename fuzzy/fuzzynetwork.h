#ifndef FUZZYNETWORK_H
#define FUZZYNETWORK_H

#include <vector>
#include <memory>
#include <QPainter>
#include <QSizeF>
#include <QRectF>

class FuzzyElement;
class NeuronModel;

class FuzzyNetwork
{
	public:
		FuzzyNetwork();
		~FuzzyNetwork();

		QSizeF get_size() const;

		void paint(QPainter& qp, QRectF window);

		void from_model(NeuronModel* model);

		std::vector<double> periods;

	private:
		class Layer {
			public:
				Layer();
				~Layer();

				std::vector<std::shared_ptr<FuzzyElement> > elements;
				bool dense;
				double u;

				QSizeF get_size() const;
				void paint(QPainter& qp, QRectF window);
				void paint_lines(QPainter& qp);
				void arange(qreal y, qreal target_width);
				void all_visible();
				void propagate_invisibility();
				void propagate_flow();
				void reset_flow();
				void calculate();
				QPointF logpoint(double x, double y, double ym, double w_) const;
				qreal get_spacing_above();
		};

		std::vector<std::shared_ptr<Layer> > layers;

		void arange();
		void optimise_crossings();
		void all_visible();
		void propagate_invisibility();
		void propagate_flow();
		void reset_flow();
		void calculate_all();
		void dissolve_layers();
		void purge_invisible();

		static bool fe_sort(const std::shared_ptr<FuzzyElement>& e1, const std::shared_ptr<FuzzyElement>& e2);
};

#endif // FUZZYNETWORK_H
