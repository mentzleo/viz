#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include "fuzzyelement.h"

class Perceptron : public FuzzyElement
{
	public:
		Perceptron(double period_, double x_, double y_, double z_);

		virtual void set_input_signal(const Signal& signal);

		virtual void paint(QPainter& qp, QRectF window);

	private:
		double period;
		double x, y, z;
};

#endif // PERCEPTRON_H
