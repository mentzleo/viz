#ifndef CONJUNCTION_H
#define CONJUNCTION_H

#include "fuzzyelement.h"

class Conjunction : public FuzzyElement
{
	public:
		Conjunction(FuzzyElement* a, FuzzyElement* b);

		virtual void calculate_signal();

		virtual void paint(QPainter& qp, QRectF window);

		virtual void propagate_invisibility();
};

#endif // CONJUNCTION_H
